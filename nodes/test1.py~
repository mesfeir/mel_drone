#!/usr/bin/env python

"""
  voice_nav.py allows controlling a mobile base using simple speech commands.
  Based on the voice_cmd_vel.py script by Michael Ferguson in the pocketsphinx ROS package.
"""

import roslib; roslib.load_manifest('pi_speech_tutorial')
import rospy

from geometry_msgs.msg import Twist
from std_msgs.msg import String,Empty
from math import copysign
from ardrone_autonomy.msg import Navdata
from mel_drone.msg import BoundingBox
import subprocess
import tf


class voice_cmd_vel:
    def __init__(self):

	self.Yaw = 0.00 
	self.Pitch = 0.00
	self.Thrust = 0.00
        rospy.on_shutdown(self.cleanup)
        self.status = -1
        self.rate = rospy.get_param("~rate", 20)
        r = rospy.Rate(self.rate)
        self.paused = False
        
        # Initialize the Twist message we will publish.
        self.msg = Twist()
	
        # Publish the Twist message to the cmd_vel topic
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
        
        # Subscribe to the /recognizer/output topic to receive voice commands.
        rospy.Subscriber('/tld_tracked_object', BoundingBox, self.faceBound)
        
       
        self.subNavdata = rospy.Subscriber('/ardrone/navdata',Navdata,self.ReceiveNavdata)
	self.pubLand    = rospy.Publisher('/ardrone/land',Empty)
	self.pubTakeoff = rospy.Publisher('/ardrone/takeoff',Empty)
 	self.pubReset   = rospy.Publisher('/ardrone/reset',Empty)
  	self.pubCommand = rospy.Publisher('/cmd_vel',Twist)

        rospy.loginfo("Ready to receive voice commands")
        
        # We have to keep publishing the cmd_vel message if we want the robot to keep moving.
        while not rospy.is_shutdown():
            self.cmd_vel_pub.publish(self.msg)
            r.sleep()                       
    def ReceiveNavdata(self,navdata):
                # Although there is a lot of data in this packet, we're only interested in the state at the moment      
        self.status = navdata.state 
	self.rotation = navdata.rotZ
	
    def handle_turtle_pose(self,msg, turtlename):
	    br = tf.TransformBroadcaster()
	    br.sendTransform((msg.x, msg.y, 0),
		             tf.transformations.quaternion_from_euler(0, 0, -200),
		             rospy.Time.now(),
		             turtlename,
		             "world")

    def expoTrans(self, value):

        exponential =( 0.1*((1+0.1)**value))
        return exponential

    def turnToThrust(self, y, imgHeight):
	heightCenter = imgHeight/2 
	distToCenter = heightCenter - y 
	thrustValue = float(((distToCenter * 2)/3))
	thrustPerc = thrustValue/100 
	if (thrustPerc < 0.15) and (thrustPerc > -0.15):
		thrustPerc = 0 
	return thrustPerc

    def turnToYaw(self, x,imgWidth):
	
	widthCenter = imgWidth/2 
	#print widthCenter
	distToCenter = widthCenter - x 
	yawValue = float(((distToCenter * 2)/3))
	yawPerc = yawValue/100 
	if (yawPerc < 0.15) and (yawPerc > -0.15):
		yawPerc = 0 
	return yawPerc

    def turnToPitch(self,width,height,distToFace):
	
	closeDistance = distToFace 
	widthToDistance = width - closeDistance
	pitchValue = float(widthToDistance)
	return pitchValue/100 
    
    def SetCommand(self,roll=0,pitch=0,yaw_velocity=0,z_velocity=0):
         # Called by the main program to set the current command
         self.msg.linear.x  = pitch
         self.msg.linear.y  = roll
         self.msg.linear.z  = z_velocity
         self.msg.angular.z = yaw_velocity

    def SendCommand(self):

         self.pubCommand.publish(self.msg)


    
    def faceBound(self, msg): 
	turtlename = "mel"
	command = 0  
        imageWidth = 640
	imageHeight = 240 
        xPos = msg.x
	yPos = msg.y
	width = msg.width
        height = msg.height
	distToFace = 30
	
	if (msg.confidence < 0.1):
		trackState = False 
	else:
		trackState = True 
       # rospy.loginfo("x ={0} , y={1} , width={2} , height {3}".format(xPos , yPos , width , height))
        
        if trackState : 
		currentPitch = self.turnToPitch(width, height, distToFace)
		currentYaw = self.turnToYaw(xPos, imageWidth)
		currentThrust = self.turnToThrust(yPos, imageHeight)
		rospy.loginfo("Yaw ={0} Pitch = {1} Thrust = {2}".format(currentYaw, currentPitch, currentThrust))
		self.handle_turtle_pose(msg, turtlename)
		self.Yaw = currentYaw
		self.Pitch = currentPitch
		self.Thrust = currentThrust
		if (self.Pitch > 0.5):
			self.Pitch = 0.5 
		elif (self.Pitch < -0.5):
			self.Pitch = -0.5
		elif ((self.Pitch < 0.1 ) and (self.Pitch > -0.1)):
			self.Pitch = 0 
		print self.Pitch			
		self.SetCommand(0,-self.Pitch*0.2, self.Yaw*0.5, self.Thrust*0.2)
		
		
        else :
		self.SetCommand(0,0,0,0) 
 
        if command == 'pause':
            self.paused = True
        elif command == 'continue':
            self.paused = False
            
        if self.paused:
            return       
        
        if command == 'forward':    
            self.msg.linear.x = self.speed
            self.msg.angular.z = 0
            
        elif command == 'rotate left':
            self.msg.linear.x = 0
            self.msg.angular.z = self.angular_speed
                
        elif command == 'rotate right':  
            self.msg.linear.x = 0      
            self.msg.angular.z = -self.angular_speed

	    p = subprocess.Popen(["ls", "-l", "/etc/resolv.conf"], stdout=subprocess.PIPE)
            output, err = p.communicate()
	    print "*** Running ls -l command ***\n", output            


        elif command == 'turn left':
            if self.msg.linear.x != 0:
                self.msg.angular.z += self.angular_increment
            else:        
                self.msg.angular.z = self.angular_speed
                
        elif command == 'turn right':    
            if self.msg.linear.x != 0:
                self.msg.angular.z -= self.angular_increment
            else:        
                self.msg.angular.z = -self.angular_speed
                
        elif command == 'backward':
            self.msg.linear.x = -self.speed
            self.msg.angular.z = 0
            
        elif command == 'stop': 
            # Stop the robot!  Publish a Twist message consisting of all zeros.         
            self.msg = Twist()
        
        elif command == 'faster':
            self.speed += self.linear_increment
            self.angular_speed += self.angular_increment
            if self.msg.linear.x != 0:
                self.msg.linear.x += copysign(self.linear_increment, self.msg.linear.x)
            if self.msg.angular.z != 0:
                self.msg.angular.z += copysign(self.angular_increment, self.msg.angular.z)
            
        elif command == 'slower':
            self.speed -= self.linear_increment
            self.angular_speed -= self.angular_increment
            if self.msg.linear.x != 0:
                self.msg.linear.x -= copysign(self.linear_increment, self.msg.linear.x)
            if self.msg.angular.z != 0:
                self.msg.angular.z -= copysign(self.angular_increment, self.msg.angular.z)
                
        elif command in ['quarter', 'half', 'full']:
            if command == 'quarter':
                self.speed = copysign(self.max_speed / 4, self.speed)
        
            elif command == 'half':
                self.speed = copysign(self.max_speed / 2, self.speed)
            
            elif command == 'full':
                self.speed = copysign(self.max_speed, self.speed)
            
            if self.msg.linear.x != 0:
                self.msg.linear.x = copysign(self.speed, self.msg.linear.x)

            if self.msg.angular.z != 0:
                self.msg.angular.z = copysign(self.angular_speed, self.msg.angular.z)
                
        else:
            return

        self.msg.linear.x = min(self.max_speed, max(-self.max_speed, self.msg.linear.x))
        self.msg.angular.z = min(self.max_angular_speed, max(-self.max_angular_speed, self.msg.angular.z))

    def cleanup(self):
        # When shutting down be sure to stop the robot!  Publish a Twist message consisting of all zeros.
        twist = Twist()
        self.cmd_vel_pub.publish(twist)

if __name__=="__main__":
    rospy.init_node('voice_nav')
    #try:
    voice_cmd_vel()
   # except:
	#print "pass"
        #pass

